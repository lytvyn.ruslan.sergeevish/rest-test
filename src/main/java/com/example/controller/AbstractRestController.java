package com.example.controller;

import com.example.entity.AbstractEntity;
import com.example.service.SimpleRestService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.*;

public abstract class AbstractRestController<E extends AbstractEntity, S extends SimpleRestService> {

    @Autowired
    private S service;

    @RequestMapping(path={"", "/"}, method = GET)
    public Iterable<? extends E> getAllItems(){
        return service.getAllItems();
    }

   @RequestMapping(path={"", "/"}, method = DELETE)
    public ResponseEntity removeItemById(@RequestParam long id){
        service.removeItemById(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path={"", "/"}, method = {POST, PUT})
    public ResponseEntity createOrUpdateItem(@RequestBody E item){

       service.createOrUpdateItem(item);
      return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getById(@RequestParam Long id) {

         service.getById(id);
        return ResponseEntity.ok().build();
    }
}



