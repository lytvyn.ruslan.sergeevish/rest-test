package com.example.controller;

import com.example.entity.User;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/userservice")
public class UserController extends AbstractRestController<User,UserService> {

    private boolean bool;
    @Autowired
    UserService service;

    @RequestMapping(path = {"", "/register"}, method = RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody User user) {

 String  json = "{\"code\" : \"USER_ALREADY_EXISTS\" , \"description\":\"A user with the given username already exists\"}";
        HttpHeaders headers = new HttpHeaders( );
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (equalsUser(user)) {

            return ResponseEntity.status(409).headers(headers).body(json);
        }else {
            user = service.createOrUpdateItem(user);
            return ResponseEntity.ok().body(user);
    }
    }

    private boolean equalsUser( User user ){

        Iterable<? extends User> listUsers = service.getAllItems();

        for (User users : listUsers) {

            if (users.getUserName().equals(user.getUserName())) {
              bool = true;
              break;
            }
            bool = false;

    } return bool;
    }
}