package com.example.service;

import com.example.entity.User;
import com.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements SimpleRestService<User>{

    @Autowired
    UserRepository userRepository;
    @Override
    public Iterable<? extends User> getAllItems() {

        return userRepository.findAll();
    }

    @Override
    public void removeItemById(long id) {
        userRepository.delete(id);

    }

    @Override
    public User createOrUpdateItem(User item) {

        return userRepository.save(item);
    }

    @Override
    public User getById(Long id) {

        return userRepository.findOne(id);
    }

}
