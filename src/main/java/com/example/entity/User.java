package com.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@NoArgsConstructor
//@AllArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User extends AbstractEntity{


	private String firstName;
	private String lastName;
	private String userName;
	@JsonIgnore
	private String plainTextPassword;
	@JsonIgnore
	private String hashedPassword;

	public  User(){

	}
	public User( String firstName, String lastName, String userName, String plainTextPassword, String hashedPassword) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.plainTextPassword = plainTextPassword;
		this.hashedPassword = hashedPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPlainTextPassword() {
		return plainTextPassword;
	}

	public void setPlainTextPassword(String plainTextPassword) {
		this.plainTextPassword = plainTextPassword;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
}

